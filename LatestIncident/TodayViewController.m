//
//  TodayViewController.m
//  LatestIncident
//
//  Created by Craig Vanderzwaag on 9/27/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;
@property (strong, nonatomic) IBOutlet UILabel *incTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *incAddressLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeigthConstraint;
@property (strong, nonatomic) IBOutlet UIButton *toggleMapView;
@property (strong, nonatomic) IBOutlet MKMapView *incidentMap;

@property (assign, nonatomic) BOOL graphVisible;

@end

@implementation TodayViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setPreferredContentSize:CGSizeMake(0, 70)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mapViewHeigthConstraint.constant = 0;
    
    // Do any additional setup after loading the view from its nib.
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets {
    return UIEdgeInsetsZero;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleMap:(id)sender {
    if (self.graphVisible) {
        _mapViewHeigthConstraint.constant = 0;
        _toggleMapView.transform = CGAffineTransformMakeRotation(0);
        [self setPreferredContentSize:CGSizeMake(0, 70.0)];
        self.graphVisible = NO;
    } else {
        _mapViewHeigthConstraint.constant = 110;
        _toggleMapView.transform = CGAffineTransformMakeRotation(180.0 * M_PI/180.0);
        [self setPreferredContentSize:CGSizeMake(0, 180.0)];
        self.graphVisible = YES;
    }
}



- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

@end
