//
//  TodayViewController.h
//  LatestIncident
//
//  Created by Craig Vanderzwaag on 9/27/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TodayViewController : UIViewController <MKMapViewDelegate>

@end
