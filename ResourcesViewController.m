//
//  ResourcesViewController.m
//  rdSTB
//
//  Created by Craig Vanderzwaag on 11/17/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "ResourcesViewController.h"
#import "ResourcesTableViewCell.h"
#import "SMXMLDocument.h"
#import "Incident.h"
#import "CHPViewController.h"

@interface ResourcesViewController ()

@property (nonatomic, strong) IBOutlet UITableView *resourcesTableView;
@property (nonatomic, strong) IBOutlet UITableView *supportTableView;
@property (nonatomic, strong) NSArray *resourcesList;
@property (nonatomic, strong) NSArray *resourceListImages;
@property (nonatomic, strong) NSArray *supportList;

//CHP Objects
@property (nonatomic, strong) NSMutableArray *items;


@end

@implementation ResourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _resourcesList = @[@"Station Locations", @"Social Media", @"SB CHP Dispatch"];
    _resourceListImages = @[@"building", @"HTIcon", @"CHPIcon"];
    _supportList = @[@"Report an Error", @"About"];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getCHPData];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CHP Data

- (void)getCHPData {
    
    _items = nil;
    _items = [[NSMutableArray alloc] init];
    
    NSURL *url = [[NSURL alloc] initWithString:@"http://media.chp.ca.gov/sa_xml/sa.xml"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    /*NSArray* slccItems = */[self _getItemsByName:@"SLCC" fromData:data];
    /*NSArray* vtccItems = */[self _getItemsByName:@"VTCC" fromData:data];
    //*NSArray* laccItems = */[self _getItemsByName:@"LACC" fromData:data];
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    // [_incidentTableView reloadData];
    
    /*
     NSUInteger count = [self.items count];
     if (count == 0) {
     statusLabel.hidden = NO;
     } else {
     statusLabel.hidden = YES;
     }
     */
    
    
    //  [MBProgressHUD hideHUDForView:self.view animated:YES];
    //     });
    //   });
    
}

- (NSArray*)_getItemsByName:(NSString*)name fromData:(NSData*)data;
{
    NSError *error;
    SMXMLDocument *document = [SMXMLDocument documentWithData:data error:&error];
    
    // check for errors
    if (error) {
        NSLog(@"Error while parsing the document: %@", error);
        return nil;
    }
    
    
    
    // For each "Center" get a "Dispatch" item and look for one called "SLCC."
    for (SMXMLElement *center in [document.root childrenNamed:@"Center"]) {
        for (SMXMLElement *dispatch in [center childrenNamed:@"Dispatch"]) {
            // Compare the dispatch id to "name".
            NSString *dispatchIdAsString = [dispatch.attributes objectForKey:@"ID"];
            if (NSOrderedSame == [dispatchIdAsString compare:name]) {
                //   NSLog(@">>>> Hey, Craig, LOOK A NODE NAMED \"%@\"!\n", name);
                for (SMXMLElement *log in [dispatch children]) {
                    // Create an incident.
                    //
                    Incident *inc = [[Incident alloc] initFromXML:log];
                    
                    // I've overriden the description method on Incident so this
                    // will print out.
                    //
                       NSLog(@"\n>> Incident: %@\n", inc);
                    
                    
                    if ([inc.area rangeOfString:@"SL"].location != NSNotFound) {
                        [_items addObject:inc];
                    }
                    if ([inc.area rangeOfString:@"Santa Barbara"].location != NSNotFound) {
                        [_items addObject:inc];
                    }
                   
                    if ([inc.area rangeOfString:@"Buellton"].location != NSNotFound) {
                        [_items addObject:inc];
                    }
                  /*
                     if ([inc.area rangeOfString:@"Ventura"].location != NSNotFound) {
                     [_items addObject:inc];
                     }
                    
                    
                     if ([inc.area rangeOfString:@"San Luis Obispo"].location != NSNotFound) {
                     [_items addObject:inc];
                     }
                     
                     
                     if ([inc.area rangeOfString:@"Altadena"].location != NSNotFound) {
                     [items addObject:inc];
                     }
                     
                     if ([inc.area rangeOfString:@"San Luis Obispo"].location != NSNotFound) {
                     [items addObject:inc];
                     }
                     
                     if ([inc.area rangeOfString:@"West LA"].location != NSNotFound) {
                     [items addObject:inc];
                     }
                     
                     if ([inc.area rangeOfString:@"Templeton"].location != NSNotFound) {
                     [items addObject:inc];
                     }
                     if ([inc.area rangeOfString:@"Ventura"].location != NSNotFound) {
                     [items addObject:inc];
                     }
                     
                     */
                    
                    
                    // Add it to our array
                    
                    //   [items addObject:inc];
                    //    NSLog(@"ITEMS ARRAY --> %@", _items);
                }
            }
            else {
                //   NSLog(@">> Sorry, Craig, it's not \"%@\": %@\n", name, dispatchIdAsString);
            }
        }
    }
    
    return _items;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == 1001) {
        
        return [_resourcesList count];
        
    } else {
        
        return [_supportList count];
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView.tag == 1001) {
        ResourcesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

            cell.rowLabel.text = [_resourcesList objectAtIndex:indexPath.row];
            cell.icon.image = [UIImage imageNamed:[_resourceListImages objectAtIndex:indexPath.row]];
    
    
        cell.backgroundColor = cell.contentView.backgroundColor;
        return cell;
    }
    else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        cell.textLabel.text = [_supportList objectAtIndex:indexPath.row];
        cell.backgroundColor = cell.contentView.backgroundColor;
        return cell;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 1001) {

            
            if (indexPath.row == 0) {
                
                [self performSegueWithIdentifier:@"stationMap" sender:self];
            //    [self logEvent:@"Did Touch Station Map"];
            } else
            if (indexPath.row == 1){
                [self showSocialAlertController];
                
            //    [self logEvent:@"Did Touch Social Controller"];
            } else{
                [self performSegueWithIdentifier:@"chp" sender:self];
            //    [self logEvent:@"Did Touch Public CHP"];
            }
    }
    
            else {
                
                if (indexPath.row == 0) {
                    [self performSegueWithIdentifier:@"reportError" sender:self];
                } else {
                    [self performSegueWithIdentifier:@"about" sender:self];
                }
            }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"chp"]) {
        CHPViewController *destVC = segue.destinationViewController;
        destVC.activeIncidents = _items;
    }
}


-(void)showSocialAlertController {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Find us on social media!"
                                          message:@"Like us on Facebook and follow us on Twitter!"
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *openFBAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Facebook", @"Facebook")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       // open the Facebook App
                                       if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/103556384150"]]) {
                                           
                                           // opening the app didn't work - let's open Safari
                                           if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/santabarbaracityfire"]]) {
                                               
                                               // neither
                                               NSLog(@"Unable to open FB");
                                           }
                                       }
                                       NSLog(@"open FB action");
                                   }];
    
    UIAlertAction *openTWAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Twitter", @"Twitter")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=SBCityFirePIO"]]) {
                                           
                                           if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/sbcityfirepio"]]) {
                                               
                                               // neither
                                               NSLog(@"Unable to open Twitter");
                                           }
                                       }
                                           NSLog(@"open Twitter action");
                                    }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:openFBAction];
    [alertController addAction:openTWAction];
    [alertController addAction:cancelAction];
    
    alertController.popoverPresentationController.sourceView = self.view;
    alertController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [alertController.popoverPresentationController setPermittedArrowDirections:0];
        // this is the center of the screen currently but it can be any point in the view
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
