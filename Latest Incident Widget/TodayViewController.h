//
//  TodayViewController.h
//  Latest Incident Widget
//
//  Created by Craig Vanderzwaag on 9/26/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TodayViewController : UIViewController <MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *unitAssigned;
@property (strong, nonatomic) IBOutlet UILabel *incidentType;
@property (strong, nonatomic) IBOutlet UILabel *incidentAddress;
@property (strong, nonatomic) IBOutlet MKMapView *incidentMap;
@property (strong, nonatomic) IBOutlet UIButton *mapButton;


@end
