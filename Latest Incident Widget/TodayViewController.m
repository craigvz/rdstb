//
//  TodayViewController.m
//  Latest Incident Widget
//
//  Created by Craig Vanderzwaag on 9/26/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import <Parse/Parse.h>
#import "CallLocation.h"

#define METERS_PER_MILE 1609.344

@interface TodayViewController () <NCWidgetProviding>

@property (assign, nonatomic) BOOL mapVisible;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeigthConstraint;

@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lon;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *callType;

@end

@implementation TodayViewController

- (void)awakeFromNib {
    [super awakeFromNib];
 
    [self setPreferredContentSize:CGSizeMake(0, 60)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Parse set up
    [Parse setApplicationId:@"Pc0DIOOSe189vXUMKdIXpexVtBIqxYxufnZyV9zG"
                  clientKey:@"TamOLZGck7JiSFOdDqsE1BQULwN5oXQalannE2Ff"];

    
    [self queryIncidents];
    
    _mapViewHeigthConstraint.constant = 0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
  //  [self updateWithCurrencyData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
  //  [self updatePriceHistoryGraph];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleGraph:(id)sender {
    if (_mapVisible) {
        _mapViewHeigthConstraint.constant = 0;
        _mapButton.transform = CGAffineTransformMakeRotation(0);
        [self setPreferredContentSize:CGSizeMake(0, 60.0)];
        _mapVisible = NO;
    } else {
        _mapViewHeigthConstraint.constant = 98;
        _mapButton.transform = CGAffineTransformMakeRotation(180.0 * M_PI/180.0);
        [self setPreferredContentSize:CGSizeMake(0, 180.0)];
        _mapVisible = YES;
    }
}

-(void)queryIncidents {
    
//    *unitAssigned;
//    *incidentType;
//    *incidentAddress;
    
    
        
        PFQuery *query = [PFQuery queryWithClassName:@"IncidentDetails"];
        [query orderByDescending:@"createdAt"];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!object) {
                NSLog(@"The getFirstObject request failed.");
            } else {
                // The find succeeded.
                NSLog(@"Successfully retrieved the object.");
                NSLog(@"Object %@", object);
                
                NSRange range = [[object objectForKey:@"unitID"] rangeOfString:@","];
                if (range.location != NSNotFound)
                {
                    NSArray *strings = [[object objectForKey:@"unitID"] componentsSeparatedByString:@","];
                    _unit = [[strings objectAtIndex:[strings count]-1] stringByReplacingOccurrencesOfString:@" " withString:@""];
                } else {
                    _unit = [[object objectForKey:@"unitID"] stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                
                _address = [object objectForKey:@"address1"];
                _callType = [object objectForKey:@"incidentType"];
                
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                
                if ([object objectForKey:@"lat"] == nil) {
                    NSString *addressToGeocode = [NSString stringWithFormat:@"%@,Santa Barbara,CA", _address];
                    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                    [geocoder geocodeAddressString:addressToGeocode completionHandler:^(NSArray* placemarks, NSError* error){
                        for (CLPlacemark* aPlacemark in placemarks)
                        {
                            // Process the placemark.
                            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
                            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
                            _lat = [f numberFromString:latDest1];
                            _lon = [f numberFromString:lngDest1];
                         //   _annotLat = _lat;
                         //   _annotLon = _lon;
                            NSLog(@"Lat %@", _lat);
                            NSLog(@"Lng %@", _lon);
                            
                        }
                                                for (id<MKAnnotation> annotation in _incidentMap.annotations) {
                            [_incidentMap removeAnnotation:annotation];
                        }
                        
                        NSNumber * latitude = _lat;
                        NSNumber * longitude = _lon;
                        NSString * name = _callType;
                        NSString * address = _address;
                        NSString * unit = _unit;
                        
                        CLLocationCoordinate2D coordinate;
                        coordinate.latitude = latitude.doubleValue;
                        coordinate.longitude = longitude.doubleValue;
                        
                        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
                        
                        [_incidentMap setRegion:viewRegion animated:YES];
                        
                     //   CallLocation *annotation = [[CallLocation alloc] initWithName:name address:address city:nil unit:unit coordinate:coordinate];
                        
                        
              //          [_incidentMap performSelector:@selector(addAnnotation:) withObject:annotation afterDelay:1];
                     //   [_incidentMap performSelector:@selector(selectAnnotation:animated:) withObject:annotation afterDelay:1.5];
                        
                    }];
                    
                } else {
                    
                    _lat = [object objectForKey:@"lat"];
                    _lon = [object objectForKey:@"lon"];
                //    _annotLat = _lat;
                //    _annotLon = _lon;
                    /*
                     DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
                     controller.unit = _unit;
                     controller.incAddress = _incAddress;
                     controller.incCity = _incCity;
                     controller.time = _time;
                     controller.incNumber = _incNumber;
                     controller.annotLat = _lat;
                     controller.annotLon = _lon;
                     controller.incType = _incType;
                     [controller setDetailItem:object];
                     
                     controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
                     controller.navigationItem.leftItemsSupplementBackButton = YES;
                     */
                    for (id<MKAnnotation> annotation in _incidentMap.annotations) {
                        [_incidentMap removeAnnotation:annotation];
                    }
                    
                    NSLog(@"Lat %@", _lat);
                    NSLog(@"Lng %@", _lon);
                    NSNumber * latitude = _lat;
                    NSNumber * longitude = _lon;
                    NSString * name = _callType;
                    NSString * address = _address;
                    NSString * unit = _unit;
                    
                    CLLocationCoordinate2D coordinate;
                    coordinate.latitude = latitude.doubleValue;
                    coordinate.longitude = longitude.doubleValue;
                    
                    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
                    
                    [_incidentMap setRegion:viewRegion animated:YES];
                    
              //      CallLocation *annotation = [[CallLocation alloc] initWithName:name address:address city:nil unit:unit coordinate:coordinate];

                    
              //      [_incidentMap performSelector:@selector(addAnnotation:) withObject:annotation afterDelay:1];
                 //   [_incidentMap performSelector:@selector(selectAnnotation:animated:) withObject:annotation afterDelay:1.5];
                    
                }
                
                [self refreshUI];
                //   [self addIncidentAnnotation];
                
            }
        }];
    }


-(void)refreshUI {
    
    _unitAssigned.text = _unit;
    _incidentAddress.text = _address;
    _incidentType.text = _callType;
    
}

/*
- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets {
    return UIEdgeInsetsZero;
}
*/

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

@end
