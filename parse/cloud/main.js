Parse.Cloud.beforeSave ('IncidentDetails', function(request, response) {
                        
                       
                       var address = request.object.get("address1");

                        if( address.indexOf('-') >= 0){
                            var addressWithUnit = address.split("-");
                            var unitNumber = addressWithUnit[0];
                            var amendedAddress = addressWithUnit[1];
                            request.object.set("addressUnit", unitNumber);
                            request.object.set("address1", amendedAddress);
                            amendedAddress = amendedAddress.replace(/ /g, "+");
                        
                        } else if ( address.indexOf('/') >= 0){
                        
                            var addressIntersection = address.replace("/", "and");
                            var formattedAddress = addressIntersection.replace("  ", "");
                            if  (address.indexOf('Hwy') >= 0) {
                        
                            var amendedAddress = formattedAddress.replace("Hwy ", "US-");
                        amendedAddress = amendedAddress.replace("Nb", "");
                        amendedAddress = amendedAddress.replace("Sb", "");
                        amendedAddress = amendedAddress.replace("On", "");
                        amendedAddress = amendedAddress.replace("Off", "");
                             amendedAddress = amendedAddress.replace(/ /g, "+");
                        } else {
                            var amendedAddress = formattedAddress.replace(/ /g, "+");
                        }
                        
                        } else {
                        
                        var amendedAddress = address.replace(/ /g, "+");
                        
                        }
                        
                       var apikey = "AIzaSyBnOzfsZku7NRQfQYXdyFowCaxn_WaOQQ8";
                       var city = "+Santa+Barbara"
                       var state = "+CA"
                        console.log("SUBMITTED Address String " + amendedAddress+city+state+'&sensor=false');

            Parse.Cloud.httpRequest({
                   
                        url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+amendedAddress+city+state+'&key='+apikey,
                                    
                        success: function(httpResponse) {
                                
                        var geocodeResponse = JSON.parse(httpResponse.text);
                                    
                            if(geocodeResponse.status == "ZERO_RESULTS"){
                                    console.log("Nothing here");
                                    request.object.set("address1", addressIntersection);
                                    response.success();
                                    
                                } else {
                                    
                                    var latitude = geocodeResponse.results[0].geometry.location.lat;
                                    var longitude = geocodeResponse.results[0].geometry.location.lng;
                                    var neighborhood = geocodeResponse.results[0].address_components[2].long_name;
                        
                                    console.log(latitude);
                                    console.log(longitude);
                                    console.log(neighborhood);
                                    
                                    request.object.set("lat", latitude);
                                    request.object.set("lon", longitude);
                                    request.object.set("neighborhood", neighborhood);
                                    response.success();
                                        }

                                    },
                        error: function(httpResponse) {
                        console.error('Request failed with response code ' + httpResponse.status);

                        }
                        });
                       
                       });



Parse.Cloud.afterSave ('IncidentDetails',function (request) {
                       
                       Parse.Push.send({
                                       channels: request.object.get("channels"),
                                       data: {
                                       alert: [request.object.get("incidentType")] + ", " + [request.object.get("address1")] + ", " + [request.object.get("neighborhood")]+ ", " + [request.object.get("unitID")],
                                       lat: request.object.get("lat"),
                                       lon: request.object.get("lon"),
                                       sound: "SBCPushAlert.wav"
                                       }
                                       }, {
                                       success: function() {
                                       // Push was successful
                                       },
                                       error: function(error) {
                                       // Handle error
                                       throw "Got an error " + error.code + " : " + error.message;
                                       }
                                       });
                       });
