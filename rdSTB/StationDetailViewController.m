//
//  StationDetailViewController.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/27/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "StationDetailViewController.h"

@interface StationDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *stationImageView;

@end

@implementation StationDetailViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    _stationImageView.image = [UIImage imageNamed:@"sta1"];
    
}

@end
