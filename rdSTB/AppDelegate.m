//
//  AppDelegate.m
//  rdSTB
//
//  Created by Craig Vanderzwaag on 9/18/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "DetailViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "UIColor+CustomColor.h"


//#import <GoogleMaps/GoogleMaps.h>

@interface AppDelegate () <UISplitViewControllerDelegate, UITabBarControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    //Parse set up
    [Parse setApplicationId:@"Pc0DIOOSe189vXUMKdIXpexVtBIqxYxufnZyV9zG"
                  clientKey:@"TamOLZGck7JiSFOdDqsE1BQULwN5oXQalannE2Ff"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
    
    //Crashlytics set up
 //
   [Fabric with:@[CrashlyticsKit]];
    
   // [Crashlytics startWithAPIKey:@"9a105cd94bdddf0788fc7edc6de980a893ac7816"];
    
    //GoogleMaps API *FUTURE*
    //[GMSServices provideAPIKey:@"AIzaSyDjbNfElR04_o31xxoGmbMHfb6-7YmxY4w"];
    
    // Register for Push Notitications, if running iOS 8
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        
    } else {
        
        // If targeting OS < iOS 8
        // Register for Push Notifications before iOS 8
        /*
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
        
         UIRemoteNotificationTypeSound)];
         */
         
    }
        
    //TabBarController setup
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UISplitViewController *firtSplitViewController = [sb instantiateViewControllerWithIdentifier:@"dispatch"];
    UINavigationController *secondViewController = [sb instantiateViewControllerWithIdentifier:@"resources"];
    
    _tabBarController = [[UITabBarController alloc] initWithNibName:nil bundle:nil];
    _tabBarController.viewControllers = [NSArray arrayWithObjects:firtSplitViewController, secondViewController, nil];
    
    _window.rootViewController = _tabBarController;
    
    self.tabBarController = (UITabBarController *)self.window.rootViewController;
    UISplitViewController *splitViewController = [self.tabBarController.viewControllers objectAtIndex:0];
    UINavigationController *navigationController = [splitViewController.viewControllers objectAtIndex:0];
    splitViewController.delegate = (id)navigationController.topViewController;
    
    [navigationController setNavigationBarHidden:NO animated:YES];
    
    //UI Customization
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[navigationController navigationBar] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [UIColor whiteColor],
                                                            NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f]
                                                            }];
    [[navigationController navigationBar] setTranslucent:YES];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                          NSForegroundColorAttributeName: [UIColor whiteColor],
                                                          NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f]
                                                          } forState:UIControlStateNormal];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithR:23 G:23 B:23 A:1]];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithR:23 G:23 B:23 A:1]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];

    
    
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    NSArray *subscribedChannels = [[NSArray alloc] initWithArray:currentInstallation.channels];
    NSMutableArray *channelsToAdd = [NSMutableArray new];
    [channelsToAdd addObjectsFromArray:subscribedChannels];
    
    //Check for global channel subscription
    if (![subscribedChannels containsObject: @"global"])
    {
        [channelsToAdd addObject:@"global"];
    }
    
    currentInstallation.channels = channelsToAdd;
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [PFPush handlePush:userInfo];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]] && ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
        
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
        
    } else {
        
        return NO;
    }
}


@end
