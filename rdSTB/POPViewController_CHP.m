//
//  POPViewController_CHP.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/3/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "POPViewController_CHP.h"

#define METERS_PER_MILE 1609.344

@implementation POPViewController_CHP

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addAnnotation];
    
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAnnotation {
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = _lat;
    coordinate.longitude = _lon;
    
    NSLog(@"COORDINATE_CHP --> %f", coordinate.latitude);
    NSLog(@"COORDINATE_CHP --> %f", coordinate.longitude);
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 1.5*METERS_PER_MILE, 1.5*METERS_PER_MILE);
    
    [_incidentMapView setRegion:viewRegion animated:YES];
    
    // Place a single pin
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    NSString *annotTitle = _address;
    [annotation setTitle:annotTitle]; //You can set the subtitle too
    [_incidentMapView addAnnotation:annotation];
    [_incidentMapView setSelectedAnnotations:@[annotation]];
    
    
}


@end
