//
//  CHPDetailViewController.h
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/2/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Incident.h"

@interface CHPDetailViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *incType;
@property (nonatomic, strong) IBOutlet UILabel *incLoc;
@property (nonatomic, strong) IBOutlet UITableView *incDetailTableView;

@property (nonatomic, strong) NSMutableArray *details;
@property (nonatomic, strong) NSMutableArray *times;

@property (nonatomic, strong) Incident* incident;

@end
