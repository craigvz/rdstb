//
//  PushSettingsViewController.h
//  rdSTB
//
//  Created by Craig Vanderzwaag on 9/24/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h> 

@interface PushSettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *unitList;
@property (nonatomic, retain) IBOutlet UINavigationBar *navBar;

@property (nonatomic, retain) IBOutlet UISegmentedControl *callTypeSegControl;
@property (nonatomic, retain) IBOutlet UITableView *stationList;
@property (nonatomic, retain) IBOutlet UITableView *incidentTypeList;

@end
