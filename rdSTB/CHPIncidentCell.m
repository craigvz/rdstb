//
//  CHPIncidentCell.m
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/7/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "CHPIncidentCell.h"

@implementation CHPIncidentCell

@synthesize location;
@synthesize logTime;
@synthesize logType;
@synthesize area;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
