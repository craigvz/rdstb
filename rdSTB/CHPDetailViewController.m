//
//  CHPDetailViewController.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/2/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "CHPDetailViewController.h"
#import "IncidentDetailCell.h"
#import "IncidentDetail.h"

@interface CHPDetailViewController ()

@end

@implementation CHPDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _incType.text = _incident.logType;
    _incLoc.text = _incident.location;
    _incDetailTableView.estimatedRowHeight = 100;
    _incDetailTableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_incident.incidentDetails count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IncidentDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    IncidentDetail *detail = [_incident.incidentDetails objectAtIndex:indexPath.row];
    
    NSString* formattedLogEntry = [detail.detail stringByReplacingOccurrencesOfString: @"\\[[\\w]+\\]"
                                                       withString: @""
                                                          options: NSRegularExpressionSearch
                                                            range: NSMakeRange(0, detail.detail.length)];
    
    
  //  Incident* incidentInfo = [_activeIncidents objectAtIndex:indexPath.row];
    
    // Fill out the cell data.
    //  cell.area.text = incidentInfo.area;
    cell.detailText.text = formattedLogEntry;
    cell.detailTime.text = detail.time;
    //  cell.logTime.text = incidentInfo.logTime;
    
    return cell;
    
    
    // Configure the cell...
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
