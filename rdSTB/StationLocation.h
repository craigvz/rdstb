//
//  StationLocation.h
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/27/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface StationLocation : MKAnnotationView

- (id)initWithName:(NSString*)name address:(NSString*)address city:(NSString*)city icon:(NSString*)icon coordinate:(CLLocationCoordinate2D)coordinate phone:(NSString*)phone;

@property (nonatomic, strong) NSString *staID;
@property (copy) NSString *stationName;
@property (copy) NSString *address;
@property (copy) NSString *phone;

@end
