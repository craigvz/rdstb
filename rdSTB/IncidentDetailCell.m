//
//  IncidentDetailCell.m
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/8/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "IncidentDetailCell.h"

@implementation IncidentDetailCell

@synthesize detailText;
@synthesize detailTime;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
            }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}





@end
