//
//  UIColor+CustomColor.h
//  emsguide
//
//  Created by Craig VanderZwaag on 12/1/12.
//  Copyright (c) 2012 blueHulaStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha;

@end