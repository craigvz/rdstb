//
//  CHPIncTableViewCell.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 9/22/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "CHPIncTableViewCell.h"

@implementation CHPIncTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
