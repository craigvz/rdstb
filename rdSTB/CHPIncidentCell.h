//
//  CHPIncidentCell.h
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/7/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHPIncidentCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *logType;
@property (nonatomic, retain) IBOutlet UILabel *logTime;
@property (nonatomic, retain) IBOutlet UILabel *area;
@property (nonatomic, retain) IBOutlet UILabel *location;

@end
