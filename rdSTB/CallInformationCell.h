//
//  CallInformationCell.h
//  emsguide
//
//  Created by Craig VanderZwaag on 1/6/12.
//  Copyright (c) 2012 blueHulaStudios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CallInformationCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *city;
@property (nonatomic, retain) IBOutlet UILabel *type;
@property (nonatomic, retain) IBOutlet UILabel *time;
@property (nonatomic, retain) IBOutlet UILabel *incNumber;

@property (nonatomic, retain) IBOutlet UILabel *address;
@property (nonatomic, strong) IBOutlet UILabel *unitID;
@property (nonatomic, strong) IBOutlet UILabel *occNumber;
@property (nonatomic, strong) IBOutlet UILabel *unitNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;


@end
