//
//  NSString+Blank.m
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/9/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "NSString+Blank.h"
#import "CHPConst.h"

@implementation NSString (NSStringBlank)

- (NSString*)stringByReplacingOccurrencesOfStringWithBlank:(NSString*)replace
{
    return [self stringByReplacingOccurrencesOfString:replace withString:kBlank];
}

@end
