//
//  CHPConst.h
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/8/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//
//#import <Foundation/Foundation.h>


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

extern NSString* const  kLatLngSeparator;
extern NSString* const  kQuoteMark;
extern NSString* const  kBlank;
extern NSString* const  kDash;
