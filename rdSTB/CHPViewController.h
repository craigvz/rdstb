//
//  CHPViewController.h
//  rdfiv
//
//  Created by Craig Vanderzwaag on 9/22/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHPViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *activeIncidents;

@end
