//
//  POPViewController_CHP.h
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/3/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface POPViewController_CHP : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *incidentMapView;
@property (nonatomic, assign) CLLocationDegrees lat;
@property (nonatomic, assign) CLLocationDegrees lon;
@property (nonatomic, strong) NSString *address;

@end
