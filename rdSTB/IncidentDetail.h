//
//  IncidentDetail.h
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/2/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMXMLDocument.h"

@interface IncidentDetail : NSObject

@property (nonatomic, retain) NSString *time;
@property (nonatomic, retain) NSString *detail;

- (id)initFromXML:(SMXMLElement*)detailXML;

@end
