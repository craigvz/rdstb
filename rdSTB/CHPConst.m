//
//  CHPConst.m
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/8/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//
#import "CHPConst.h"

NSString* const  kLatLngSeparator    = @":";
NSString* const  kQuoteMark          = @"\"";
NSString* const  kBlank              = @"";
NSString* const  kDash               = @"-";
