//
//  CHPIncTableViewCell.h
//  rdfiv
//
//  Created by Craig Vanderzwaag on 9/22/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHPIncTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *logType;
@property (nonatomic, retain) IBOutlet UILabel *logTime;
@property (nonatomic, retain) IBOutlet UILabel *area;
@property (nonatomic, retain) IBOutlet UILabel *location;

@end
