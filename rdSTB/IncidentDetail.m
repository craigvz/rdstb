//
//  IncidentDetail.m
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/2/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "IncidentDetail.h"
#import "SMXMLDocument.h"
#import "CHPConst.h"


@implementation IncidentDetail
@synthesize time;
@synthesize detail;

- (id)initFromXML:(SMXMLElement*)detailXML
{
    NSAssert(detailXML, @"incidentDetailFromXML() was given a nil detailXML pointer, this is bad\n");
    NSAssert(NSOrderedSame == [[detailXML name] compare:@"details"], @"incidentDetailFromXML() was given an invalid type, expecting a \"details\" element, received %@\n", detailXML.name);
    
    if ((self = [super init]))
    {
        // Get the detail properties
        self.time     = [[detailXML childNamed:@"DetailTime"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        self.detail   = [[detailXML childNamed:@"IncidentDetail"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@", self.time, self.detail];
   // return [NSString stringWithFormat:@"\nTime: %@ \nDetail: %@\n", self.time, self.detail];
}

@end
