//
//  Incident.h
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/1/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SMXMLDocument.h"

@interface Incident : NSObject

@property (nonatomic, retain) NSString *logID;
@property (nonatomic, retain) NSString *logTime;
@property (nonatomic, retain) NSString *logType;
@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *area;
@property (nonatomic, assign) CLLocationDegrees latitude;
@property (nonatomic, assign) CLLocationDegrees longitude;
@property (nonatomic, retain) NSMutableArray *incidentDetails;

- (id)initFromXML:(SMXMLElement*)incidentXML;

@end
