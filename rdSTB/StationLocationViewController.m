//
//  StationLocationViewController.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/27/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#define METERS_PER_MILE 1609.344

#define STA1_LAT 34.419870
#define STA1_LON -119.703349
#define STA2_LAT 34.419274
#define STA2_LON -119.677034
#define STA3_LAT 34.429991
#define STA3_LON -119.702158
#define STA4_LAT 34.441123
#define STA4_LON -119.740245
#define STA5_LAT 34.425199
#define STA5_LON -119.732158
#define STA6_LAT 34.401378
#define STA6_LON -119.720375
#define STA7_LAT 34.443483
#define STA7_LON -119.690637
#define STA8_LAT 34.432641
#define STA8_LON -119.839129

#import "StationLocationViewController.h"
#import <MapKit/MapKit.h>
#import "LocationManager.h"
#import "StationLocation.h"

@interface StationLocationViewController () <MKMapViewDelegate, CLLocationManagerDelegate, LocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *stationMap;
@property (nonatomic, strong) NSArray *stationLocations;
@property (nonatomic, assign) CLLocationCoordinate2D station1;
@property (nonatomic, assign) CLLocationCoordinate2D station2;
@property (nonatomic, assign) CLLocationCoordinate2D station3;
@property (nonatomic, assign) CLLocationCoordinate2D station4;
@property (nonatomic, assign) CLLocationCoordinate2D station5;
@property (nonatomic, assign) CLLocationCoordinate2D station6;
@property (nonatomic, assign) CLLocationCoordinate2D station7;
@property (nonatomic, assign) CLLocationCoordinate2D station8;



@end

@implementation StationLocationViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude =   34.440578;
    zoomLocation.longitude = -119.755018;
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.2425,0.0825);
    
    MKCoordinateRegion region = MKCoordinateRegionMake(zoomLocation, span);
    
    _stationMap.region = region;
    [_stationMap setDelegate:self];
    [self addStationPins];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addStationPins
{

    //Set up annotation data
    _station1.latitude = STA1_LAT;
    _station1.longitude = STA1_LON;
    
    _station2.latitude = STA2_LAT;
    _station2.longitude = STA2_LON;
    
    _station3.latitude = STA3_LAT;
    _station3.longitude = STA3_LON;
    
    _station4.latitude = STA4_LAT;
    _station4.longitude = STA4_LON;
    
    _station5.latitude = STA5_LAT;
    _station5.longitude = STA5_LON;
    
    _station6.latitude = STA6_LAT;
    _station6.longitude = STA6_LON;
    
    _station7.latitude = STA7_LAT;
    _station7.longitude = STA7_LON;
    
    _station8.latitude = STA8_LAT;
    _station8.longitude = STA8_LON;
    
    //Create MKPinAnnotationView's
    StationLocation *sta1 = [[StationLocation alloc] initWithName:@"Station 1" address:@"121 W Carrillo St" city:@"Santa Barbara" icon:@"B1" coordinate:_station1 phone:@"8059655254"];
    StationLocation *sta2 = [[StationLocation alloc] initWithName:@"Station 2" address:@"819 Cacique St" city:@"Santa Barbara" icon:@"B1" coordinate:_station2 phone:@"8059655254"];
    StationLocation *sta3 = [[StationLocation alloc] initWithName:@"Station 3" address:@"415 E Sola St" city:@"Santa Barbara" icon:@"B1" coordinate:_station3 phone:@"8059655254"];
    StationLocation *sta4 = [[StationLocation alloc] initWithName:@"Station 4" address:@"19 N Ontare Rd" city:@"Santa Barbara" icon:@"B1" coordinate:_station4 phone:@"8059655254"];
    StationLocation *sta5 = [[StationLocation alloc] initWithName:@"Station 5" address:@"2505 Modoc Rd" city:@"Santa Barbara" icon:@"B1" coordinate:_station5 phone:@"8059655254"];
    StationLocation *sta6 = [[StationLocation alloc] initWithName:@"Station 6" address:@"1802 Cliff Dr" city:@"Santa Barbara" icon:@"B1" coordinate:_station6 phone:@"8059655254"];
    StationLocation *sta7 = [[StationLocation alloc] initWithName:@"Station 7" address:@"2411 Stanwood Dr" city:@"Santa Barbara" icon:@"B1" coordinate:_station7 phone:@"8059655254"];
    StationLocation *sta8 = [[StationLocation alloc] initWithName:@"Station 8" address:@"40 Hartley Pl" city:@"Santa Barbara" icon:@"B1" coordinate:_station8 phone:@"8059655254"];
    
    _stationLocations = [[NSArray alloc] initWithObjects:sta1, sta2, sta3, sta4, sta5, sta6, sta7, sta8, nil];
    
    [_stationMap performSelector:@selector(addAnnotations:) withObject:_stationLocations afterDelay:1.0];

   // [_stationMap performSelector:@selector(selectAnnotation:animated:) withObject:_stationLocations afterDelay:1.5];
    
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    
    
    StationLocation *pinView = (StationLocation *) [_stationMap dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    //If not already instatiated, create MKPinAnnotation
    if (!pinView)
    {
        pinView = [[StationLocation alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        pinView.image = [UIImage imageNamed:@"CustomPin"];
        pinView.canShowCallout = YES;
        pinView.centerOffset = CGPointMake(0, -pinView.image.size.height/2);
        pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [pinView setSelected:YES];
        
       
    }
    else
    {
        pinView.annotation = annotation;
    }
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    id<MKAnnotation> selectedAnnotation = view.annotation;
    
    if ([selectedAnnotation isKindOfClass:[StationLocation class]]) {
        StationLocation *staLocation = (StationLocation *)selectedAnnotation;
        NSLog(@"selected station = %@", staLocation.stationName);
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:staLocation.stationName
                                              message:staLocation.address
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *routeAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Route to Station", @"Route action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           if ([staLocation.stationName isEqualToString:@"Station 1"]) {
                                               [self routeToLocation:_station1 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 2"]){
                                               [self routeToLocation:_station2 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 3"]){
                                               [self routeToLocation:_station3 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 4"]){
                                               [self routeToLocation:_station4 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 5"]){
                                               [self routeToLocation:_station5 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 6"]){
                                               [self routeToLocation:_station6 name:staLocation.stationName];
                                           } else if ([staLocation.stationName isEqualToString:@"Station 7"]){
                                               [self routeToLocation:_station7 name:staLocation.stationName];
                                           } else {
                                               [self routeToLocation:_station8 name:staLocation.stationName];
                                           }
                                               
                                           NSLog(@"Route action");
                                       }];
        
        UIAlertAction *callAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Call Station", @"Call action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Call action");
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", staLocation.phone]]];
                                   }];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction *action)
                                     {
                                         NSLog(@"Cancel action");
                                     }];
        
        [alertController addAction:routeAction];
        [alertController addAction:callAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
}


-(void)routeToLocation:(CLLocationCoordinate2D)location name:(NSString *)name {
    
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving,};
    
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
    
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    endingItem.name = [NSString stringWithFormat:@"STB %@", name];
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];

}

@end
