//
//  MasterViewController.h
//  BHTabViewSplitVC
//
//  Created by Craig Vanderzwaag on 9/21/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@class DetailViewController;

@interface MasterViewController : PFQueryTableViewController


@property (strong, nonatomic) DetailViewController *detailViewController;




@property (nonatomic, retain) NSString *combinedLatString;
@property (nonatomic, retain) NSString *combinedLonString;
@property (nonatomic, retain) NSString *finalLonString;

@property (nonatomic, retain) NSString *callType;
@property (nonatomic, retain) NSString *callAddress;
@property (nonatomic, retain) NSString *callCity;
@property (nonatomic, retain) NSString *callTime;
@property (nonatomic, retain) NSString *incNumber;
@property (nonatomic, retain) NSString *unitAssigned;
@property (nonatomic, retain) NSString *occupancyNumber;
@property (nonatomic, retain) NSString *occupancyName;
@property (nonatomic, strong) NSString *annotIconMarker;

@property (nonatomic, retain) NSString *code20Text;

@property (nonatomic, retain) NSNumber *lat;
@property (nonatomic, retain) NSNumber *lon;

@property (nonatomic, strong) NSDate *object;


@end
