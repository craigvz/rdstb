//
//  Incident.m
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/1/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import "Incident.h"
#import "IncidentDetail.h"
#import "CHPConst.h"

static NSUInteger const kLatIdx = 0;
static NSUInteger const kLngIdx = 1;
static NSUInteger const kLatLngCount = 2;

@interface Incident (Private)
- (void)_initDetails:(SMXMLElement*)details;
- (void)_initLatLong:(NSString*)latLngStr;
- (NSString*)_cleanNumericString:(NSString*)doubleString;
@end // Incident (Private)

@implementation Incident

@synthesize logID;
@synthesize location;
@synthesize logTime;
@synthesize logType;
@synthesize area;
@synthesize latitude;
@synthesize longitude;
@synthesize incidentDetails;

- (id)initFromXML:(SMXMLElement*)incidentXML
{
    // Hey, we need something to parse, right?
    NSAssert(incidentXML, @"incidentFromXML() was given a nil incidentXML pointer, this is bad\n");
    NSAssert(NSOrderedSame == [[incidentXML name] compare:@"Log"], @"incidentFromXML() was given an invalid type, expecting a \"Log\" element, received %@\n", incidentXML.name);
 
    if ((self = [super init])) {
        // Get the incident properties
        self.logID      = [[incidentXML.attributes objectForKey:@"ID"] stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        self.location   = [[incidentXML childNamed:@"Location"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        self.logTime    = [[incidentXML childNamed:@"LogTime"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        self.area       = [[incidentXML childNamed:@"Area"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        
        // Get the logType and clean up the string a bit.
        NSString* logTypeStr = [[incidentXML childNamed:@"LogType"].value stringByReplacingOccurrencesOfString:kQuoteMark withString:kBlank];
        NSRange range = [logTypeStr rangeOfString:kDash];
        self.logType = logTypeStr;
        if (range.length)
        {
            // If the dash falls at the end of the string, grab everything to the left of it, if it's not, grab
            // everything to the right of it.
            self.logType    = (([logTypeStr length]-1) <= range.location) ?
                                        [logTypeStr substringToIndex:range.location] :
                                        [logTypeStr substringFromIndex:range.location+1];
        }

        // Convert the lat:lng value to a true lat and lng.
        [self _initLatLong:[incidentXML childNamed:@"LATLON"].value];
        
        // Now fill in the details.
        [self _initDetails:[incidentXML childNamed:@"LogDetails"]];
    }
    
    return self;
}

- (void)_initDetails:(SMXMLElement*)details
{
    // Initialize to nil
    self.incidentDetails = nil;
    
    if (details &&
        [details.children count]) {
        // Allocate a detail array
        self.incidentDetails  = [[NSMutableArray alloc] init];
        if (self.incidentDetails) {
            // Iterate over the details, create a new detail item and
            // jam it into our collection.
            for (SMXMLElement *detailElement in details.children) {
                // Make sure we check the element name before trying to create
                // an IncidentDetail becuase the details include "units" elements.
                if (NSOrderedSame == [[detailElement name] compare:@"details"])
                {
                    IncidentDetail* detail = [[IncidentDetail alloc] initFromXML:detailElement];
                    [self.incidentDetails addObject:detail];
                }
            }
        }
    }    
}

- (void)_initLatLong:(NSString*)latLngStr
{
    // Initialize to zero
    self.latitude   = 0;
    self.longitude  = 0;
    
    if (latLngStr &&
        [latLngStr length]) {
        NSLog(@"Lat Long String before parse: %@\n", latLngStr);
        NSArray* latLngItems = [latLngStr componentsSeparatedByString:kLatLngSeparator];
        if (latLngItems && kLatLngCount == [latLngItems count]) {
            NSString *latStr = [self _cleanNumericString:[latLngItems objectAtIndex:kLatIdx]];
            if (latStr && [latStr length]) {
                NSMutableString *formattedLat = [NSMutableString stringWithString:latStr];
                if (formattedLat.intValue !=0) {
                    [formattedLat insertString:@"." atIndex:2];
                }
                self.latitude = [formattedLat doubleValue];
            }
            NSString *lngStr = [self _cleanNumericString:[latLngItems objectAtIndex:kLngIdx]];
            if (lngStr && [lngStr length]) {
                NSMutableString *formattedLon = [NSMutableString stringWithString:lngStr];
                if (formattedLon.intValue !=0) {
                    [formattedLon insertString:@"." atIndex:3];
                    [formattedLon insertString:@"-" atIndex:0];
                }
               
                self.longitude = [formattedLon doubleValue];
            }
        }
    }
}

- (NSString*)_cleanNumericString:(NSString*)doubleString
{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    return [doubleString stringByTrimmingCharactersInSet:charactersToRemove];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"\nIncident ID: %@ \nLocation: %@\nTime: %@\nType: %@\nArea: %@\nLAT: %f\nLON: %f\n\tDetails: %@\n",
            self.logID, self.location, self.logTime, self.logType, self.area, self.latitude, self.longitude, self.incidentDetails];
}

@end
