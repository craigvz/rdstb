//
//  ResourcesTableViewCell.h
//  rdstb
//
//  Created by Craig Vanderzwaag on 9/22/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourcesTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *icon;
@property (nonatomic, strong) IBOutlet UILabel *rowLabel;

@end
