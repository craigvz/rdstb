//
//  PushSettingsViewController.m
//  rdSTB
//
//  Created by Craig Vanderzwaag on 9/24/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import "PushSettingsViewController.h"
#import "UIColor+CustomColor.h"
#import "SVProgressHUD.h"

@interface PushSettingsViewController ()

@property (nonatomic, strong) NSMutableArray *unitID;
@property (nonatomic, strong) NSIndexPath *checkedUnitIDIndexPath;
@property (nonatomic, strong) NSMutableArray *selectedUnitID;

@property (nonatomic, strong) NSArray *incidentCategory;
@property (nonatomic, strong) NSIndexPath *checkedCategoryIndexPath;
@property (nonatomic, strong) NSMutableArray *selectedCategory;

@property (nonatomic, strong) NSMutableArray *selectedChannels;




@end

@implementation PushSettingsViewController

- (id)initWithCoder:(NSCoder *)aCoder {
    self = [super initWithCoder:aCoder];
    if (self) {
        
    }
    return self;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _stationList.allowsMultipleSelection = YES;
    _stationList.backgroundView = nil;
    _incidentTypeList.backgroundView = nil;
    _incidentTypeList.allowsMultipleSelection = YES;
    _incidentTypeList.hidden = YES;
    [_stationList setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [_incidentTypeList setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    _selectedChannels = [[NSMutableArray alloc] initWithArray:[currentInstallation objectForKey:@"channels"]];
    NSLog(@"selectedChannels--> %@", _selectedChannels);
    
    _unitList.allowsMultipleSelection = YES;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"STBUnitID" ofType:@"plist"];
    _unitID = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    _incidentCategory = @[ @"Structure Fire", @"Wildland Fire", @"Vehicle Accident", @"Technical Rescue", @"Medical Emergencies", @"All Incidents"];
    
    if ([NSUserDefaults standardUserDefaults] != NULL) {
        _selectedUnitID = [NSMutableArray new];
        NSKeyedUnarchiver *unarchiver;
        unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"stations"]];
        _selectedUnitID = [unarchiver decodeObjectForKey:@"stations"];
        [unarchiver finishDecoding];
        [_stationList reloadData];
        self.view.backgroundColor = [UIColor blackColor];
        
    }
    
    if ([NSUserDefaults standardUserDefaults] != NULL) {
        _selectedCategory = [NSMutableArray new];
        NSKeyedUnarchiver *unarchiver;
        unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"incidents"]];
        _selectedCategory = [unarchiver decodeObjectForKey:@"incidents"];
        [unarchiver finishDecoding];
        [_incidentTypeList reloadData];
        
        
        
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    _stationList.backgroundColor = [UIColor clearColor];
    _incidentTypeList.backgroundColor = [UIColor clearColor];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
/*
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBehind:)];
    
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
    [self.view.window addGestureRecognizer:recognizer];
 */
}


- (void)handleTapBehind:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint location = [sender locationInView:nil]; //Passing nil gives us coordinates in the window
        
        //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
        
        if (!([self.view pointInside:[self.view convertPoint:location fromView:self.view.window] withEvent:nil] || [self.navigationController.view pointInside:[self.navigationController.view convertPoint:location fromView:self.navigationController.view.window] withEvent:nil]))
            
        {
            // Remove the recognizer first so it's view.window is valid.
            [self.view.window removeGestureRecognizer:sender];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

-(IBAction)didSwitchLists:(id)sender {
    
    NSLog(@"%ld", (long)_callTypeSegControl.selectedSegmentIndex);
    
    
    if (_callTypeSegControl.selectedSegmentIndex == 1) {
        _stationList.hidden = YES;
        _incidentTypeList.hidden = NO;
    }
    else {
        if (_callTypeSegControl.selectedSegmentIndex == 0) {
            _stationList.hidden = NO;
            _incidentTypeList.hidden = YES;
        }
    }
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag == 1001) {
        return _unitID.count;
    }
    return _incidentCategory.count;

}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    /*
    if (indexPath.row & 1) {
        cell.backgroundColor = [UIColor colorWithR:43 G:43 B:43 A:0.95];
    } else {
        cell.backgroundColor = [UIColor colorWithR:33 G:33 B:33 A:0.95];
    }
     */
    cell.accessoryView.backgroundColor = [UIColor clearColor];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        
    }
    
    if (_selectedUnitID == NULL) {
        
        _selectedUnitID = [NSMutableArray new];
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"stations"];
        
        if (dataRepresentingSavedArray != nil) {
            
            NSMutableArray *oldSavedStationArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
            if (oldSavedStationArray != nil)
                _selectedUnitID = [[NSMutableArray alloc] initWithArray:oldSavedStationArray];
            else
                _selectedUnitID = [[NSMutableArray alloc] init];
        }
    }
    
    if (tableView.tag != 1001) {
        
        if (_selectedCategory == NULL) {
            
            _selectedCategory = [NSMutableArray new];
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"incidents"];
            
            if (dataRepresentingSavedArray != nil) {
                
                NSMutableArray *oldSavedIncidentArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
                if (oldSavedIncidentArray != nil)
                    _selectedCategory = [[NSMutableArray alloc] initWithArray:oldSavedIncidentArray];
                else
                    _selectedCategory = [[NSMutableArray alloc] init];
            }
        }
    }
    
    
    NSUInteger row = [indexPath row];
    
    if (tableView.tag == 1001) {
        if([_selectedUnitID containsObject:indexPath]){
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    else {
        if([_selectedCategory containsObject:indexPath]){
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    
    
    if (tableView.tag == 1001) {
        cell.textLabel.text = [_unitID objectAtIndex:row];
    }
    else {
        cell.textLabel.text = [_incidentCategory objectAtIndex:row];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 1001) {
        if([_selectedUnitID containsObject:indexPath]) {
            [_selectedUnitID removeObject:indexPath];
        }
        else {
            [_selectedUnitID addObject:indexPath];
            
            NSLog(@"selected station List -->%@", _selectedUnitID);
            
        }
        
        
        NSMutableData *stationData = [NSMutableData data];
        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:stationData];
        // Customize archiver here
        [archiver encodeObject:_selectedUnitID forKey:@"stations"];
        [archiver finishEncoding];
        [[NSUserDefaults standardUserDefaults] setObject:stationData forKey:@"stations"];
        
        
        if (indexPath.row == 0) {
            if ([_selectedChannels containsObject:@"Station1"]) {
                [_selectedChannels removeObject:@"Station1"];
            } else {
                [_selectedChannels addObject:@"Station1"];
            }
        }
        
        if (indexPath.row == 1) {
            if (![_selectedChannels containsObject:@"Station2"]) {
                [_selectedChannels addObject:@"Station2"];
            } else {
                [_selectedChannels removeObject:@"Station2"];
            }
        }
        
        if (indexPath.row == 2) {
            if (![_selectedChannels containsObject:@"Station3"]) {
                [_selectedChannels addObject:@"Station3"];
            } else {
                [_selectedChannels removeObject:@"Station3"];
            }
        }
        
        if (indexPath.row == 3) {
            if (![_selectedChannels containsObject:@"Station4"]) {
                [_selectedChannels addObject:@"Station4"];
            } else {
                [_selectedChannels removeObject:@"Station4"];
            }
        }
        
        if (indexPath.row == 4) {
            if (![_selectedChannels containsObject:@"Station5"]) {
                [_selectedChannels addObject:@"Station5"];
            } else {
                [_selectedChannels removeObject:@"Station5"];
            }
        }
        
        if (indexPath.row == 5) {
            if (![_selectedChannels containsObject:@"Station6"]) {
                [_selectedChannels addObject:@"Station6"];
            } else {
                [_selectedChannels removeObject:@"Station6"];
            }
        }
        
        if (indexPath.row == 6) {
            if (![_selectedChannels containsObject:@"Station7"]) {
                [_selectedChannels addObject:@"Station7"];
            } else {
                [_selectedChannels removeObject:@"Station7"];
            }
        }
        
        if (indexPath.row == 7) {
            if (![_selectedChannels containsObject:@"Station8"]) {
                [_selectedChannels addObject:@"Station8"];
            } else {
                [_selectedChannels removeObject:@"Station8"];
            }
        }
        
        if (indexPath.row == 8) {
            if (![_selectedChannels containsObject:@"BC712"]) {
                [_selectedChannels addObject:@"BC712"];
            } else {
                [_selectedChannels removeObject:@"BC712"];
            }
        }
        
        if (indexPath.row == 9) {
            if (![_selectedChannels containsObject:@"BC713"]) {
                [_selectedChannels addObject:@"BC713"];
            } else {
                [_selectedChannels removeObject:@"BC713"];
            }
        }
        
        if (indexPath.row == 10) {
            if (![_selectedChannels containsObject:@"BC714"]) {
                [_selectedChannels addObject:@"BC714"];
            } else {
                [_selectedChannels removeObject:@"BC714"];
            }
        }
        
        if (indexPath.row == 11) {
            if (![_selectedChannels containsObject:@"BC716"]) {
                [_selectedChannels addObject:@"BC716"];
            } else {
                [_selectedChannels removeObject:@"BC716"];
            }
        }
        
        if (indexPath.row == 12) {
            if (![_selectedChannels containsObject:@"BC730"]) {
                [_selectedChannels addObject:@"BC730"];
            } else {
                [_selectedChannels removeObject:@"BC730"];
            }
        }
        
        if (indexPath.row == 13) {
            if (![_selectedChannels containsObject:@"PI732"]) {
                [_selectedChannels addObject:@"PI732"];
            } else {
                [_selectedChannels removeObject:@"PI732"];
            }
        }
        
    } else {
        
        if([_selectedCategory containsObject:indexPath]){
            [_selectedCategory removeObject:indexPath];
        } else {
            [_selectedCategory addObject:indexPath];
        }
        
        
        NSMutableData *incidentData = [NSMutableData data];
        NSKeyedArchiver *incidentArchiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:incidentData];
        // Customize archiver here
        [incidentArchiver encodeObject:_selectedCategory forKey:@"incidents"];
        [incidentArchiver finishEncoding];
        [[NSUserDefaults standardUserDefaults] setObject:incidentData forKey:@"incidents"];
        
        if (indexPath.row == 0) {
            if ([_selectedChannels containsObject:@"StructureFire"]) {
                [_selectedChannels removeObject:@"StructureFire"];
            } else {
                [_selectedChannels addObject:@"StructureFire"];
            }
        }
        
        if (indexPath.row == 1) {
            if (![_selectedChannels containsObject:@"WildlandFire"]) {
                [_selectedChannels addObject:@"WildlandFire"];
            } else {
                [_selectedChannels removeObject:@"WildlandFire"];
            }
        }
        
        if (indexPath.row == 2) {
            if (![_selectedChannels containsObject:@"VehicleAccident"]) {
                [_selectedChannels addObject:@"VehicleAccident"];
            } else {
                [_selectedChannels removeObject:@"VehicleAccident"];
            }
        }
        
        if (indexPath.row == 3) {
            if (![_selectedChannels containsObject:@"TechnicalRescue"]) {
                [_selectedChannels addObject:@"TechnicalRescue"];
            } else {
                [_selectedChannels removeObject:@"TechnicalRescue"];
            }
        }
        
        if (indexPath.row == 4) {
            if (![_selectedChannels containsObject:@"MedicalEmergency"]) {
                [_selectedChannels addObject:@"MedicalEmergency"];
            } else {
                [_selectedChannels removeObject:@"MedicalEmergency"];
            }
        }
        
        if (indexPath.row == 5) {
            if (![_selectedChannels containsObject:@"AllIncidents"]) {
                [_selectedChannels addObject:@"AllIncidents"];
            } else {
                [_selectedChannels removeObject:@"AllIncidents"];
            }
        }
    }
    
    [tableView reloadData];
    [self updateSelectedChannels];
}

-(void)updateSelectedChannels {
    
    [SVProgressHUD show];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setChannels:@[@"global"]];
    [currentInstallation addUniqueObjectsFromArray:_selectedChannels forKey:@"channels"];
    [currentInstallation saveEventually:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            [SVProgressHUD dismiss];
            NSLog(@"Successfully subscribed to AllIncidents channel.");
        } else {
            [SVProgressHUD dismiss];
            [self showSubscriptionErrorAlert];
            [_incidentTypeList reloadData];
            
        }
    }];
    
    
}

-(IBAction)didTouchDoneButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showSubscriptionErrorAlert {
    
    NSLog(@"Failed to subscribe to channel.");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                    message:@"Unable to reach the server"
                                                   delegate:self
                                          cancelButtonTitle:@"Try Your Selection Again"
                                          otherButtonTitles: nil];
    
    [alert show];
    
}



@end
