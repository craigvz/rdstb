//
//  LocationManager.h
//  emsguide
//
//  Created by Craig VanderZwaag on 1/24/13.
//  Copyright (c) 2013 blueHulaStudios. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@protocol LocationManagerDelegate

- (void)locationUpdate:(CLLocation*)location;


@end

@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, weak) id  delegate;


+ (LocationManager*)sharedManager;

@end
