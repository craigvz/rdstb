//
//  StationLocation.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/27/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "StationLocation.h"

@interface StationLocation ()


@property (copy) NSString *city;
@property (copy) NSString *icon;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;


@end

@implementation StationLocation

- (id)initWithName:(NSString*)name address:(NSString*)address city:(NSString*)city icon:(NSString*)icon coordinate:(CLLocationCoordinate2D)coordinate phone:(NSString *)phone {
    if ((self = [super init])) {
        
        _stationName = [name copy];
        _address = [address copy];
        _city = [city copy];
        _coordinate = coordinate;
        _icon = [icon copy];
        _staID = [name copy];
        _phone = [phone copy];
        
        
    }
    return self;
    
}

- (NSString *)title {
    return _stationName;
}

- (NSString *)subtitle {
    
    if (_city == nil) {
        
        NSString *combinedSubtitle = [NSString stringWithFormat:@"%@", _address];
        return combinedSubtitle;
        
    } else {
        
        NSString *combinedSubtitle = [NSString stringWithFormat:@"%@, %@", _address, _city];
        return combinedSubtitle;
        
    }
    
}

- (NSString *)pincolor{
    return _staID;
}

- (void) setpincolor:(NSString*) name{
    _staID = name;
}


@end
