//
//  CHPViewController.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 9/22/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "CHPViewController.h"
#import "CHPDetailViewController.h"
#import "CHPIncTableViewCell.h"
#import "NSString+Blank.h"
#import "SMXMLDocument.h"
#import "Incident.h"
#import "UIColor+CustomColor.h"
#import "POPViewController_CHP.h"


@interface CHPViewController () <UIViewControllerPreviewingDelegate>

@property (nonatomic, strong) IBOutlet UITableView *incidentTableView;
@property (nonatomic, strong) Incident *chpIncident;




@end

@implementation CHPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Active Inc --> %@", _activeIncidents);
  
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self check3DTouch];
    if (_activeIncidents.count == 0) {
        _incidentTableView.hidden = YES;
    }
  
}

-(void)viewDidAppear:(BOOL)animated {
    
    NSIndexPath *indexPath = [_incidentTableView indexPathForSelectedRow];
    if(indexPath) {
        [_incidentTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)check3DTouch {
    
    if ([self.traitCollection
         respondsToSelector:@selector(forceTouchCapability)] &&
        (self.traitCollection.forceTouchCapability ==
         UIForceTouchCapabilityAvailable))
    {
        [self registerForPreviewingWithDelegate:self sourceView:self.view];
        NSLog(@"3D Touch is available! Hurra!");
    }
    
    // register for 3D Touch (if available)
    
    else {
        
        NSLog(@"3D Touch is not available on this device. Sniff!");
        
        // handle a 3D Touch alternative (long gesture recognizer)
        //   self.longPress.enabled = YES;
        
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [_activeIncidents count];

    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CHPCellBack.png"]];
    tableView.backgroundColor = [UIColor colorWithR:71 G:71 B:71 A:1];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 1001) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        return cell;
    }
    
    CHPIncTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    Incident* incidentInfo = [_activeIncidents objectAtIndex:indexPath.row];
    
    //Populate Cell Data
    cell.area.text = incidentInfo.area;
    cell.logType.text = incidentInfo.logType;
    cell.location.text = incidentInfo.location;
    cell.logTime.text = incidentInfo.logTime;

    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
      //  [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        
        if ([segue.identifier isEqualToString:@"chpDetail"]) {
            NSIndexPath *indexPath = [_incidentTableView indexPathForSelectedRow];
            CHPDetailViewController *destVC = segue.destinationViewController;
            destVC.incident = [_activeIncidents objectAtIndex:indexPath.row];
        
        }

    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark Peek and Pop

- (nullable UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    // check if we're not already displaying a preview controller
    if ([self.presentedViewController isKindOfClass:[POPViewController_CHP class]]) {
        return nil;
    }
    
    // shallow press: return the preview controller here (peek)
    NSIndexPath *indexPath = [_incidentTableView indexPathForRowAtPoint:location];
    
    
    NSLog(@"INDEX PATH --> %@", indexPath);
    
    
    if (indexPath) {
        UITableViewCell *cell = [_incidentTableView cellForRowAtIndexPath:indexPath];
        _chpIncident = [_activeIncidents objectAtIndex:indexPath.row];
        /*
        _callAddress = incidentInfo.location;
        _lat = [incident objectForKey:@"lat"];
        _lon = [incident objectForKey:@"lon"];
        _callType = [incident objectForKey:@"incidentType"];
        _unitAssigned = [incident objectForKey:@"unitID"];
        _callTime = [incident objectForKey:@"incidentTime"];
        _callCity = [incident objectForKey:@"neighborhood"];
        */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        POPViewController_CHP *previewController = [storyboard instantiateViewControllerWithIdentifier:@"pop_chp"];
        previewController.lat = _chpIncident.latitude;
        previewController.lon = _chpIncident.longitude;
        previewController.address = _chpIncident.location;
        
        previewingContext.sourceRect = cell.frame;
        
        return previewController;
    }
    
    return nil;
    
}


- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    
    // deep press: bring up the commit view controller (pop)
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CHPDetailViewController *commitController = [storyboard instantiateViewControllerWithIdentifier:@"chpDetail"];
    
    commitController.incident = _chpIncident;
    
    [self showViewController:commitController sender:self];
     
}



@end
