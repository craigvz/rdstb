//
//  CallInformationCell.m
//  emsguide
//
//  Created by Craig VanderZwaag on 1/6/12.
//  Copyright (c) 2012 blueHulaStudios. All rights reserved.
//

#import "CallInformationCell.h"


@implementation CallInformationCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
    }
   
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
