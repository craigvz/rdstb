//
//  PopViewController.m
//  rdfiv
//
//  Created by Craig Vanderzwaag on 10/2/15.
//  Copyright © 2015 blueHula Studios. All rights reserved.
//

#import "PopViewController.h"

#define METERS_PER_MILE 1609.344

@implementation PopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addAnnotation];

    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAnnotation {
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = _lat.doubleValue;
    coordinate.longitude = _lon.doubleValue;
    
     NSLog(@"COORDINATE_Dispatch --> %f", coordinate.latitude);
    NSLog(@"COORDINATE_Dispatch --> %f", coordinate.longitude);
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    [_incidentMapView setRegion:viewRegion animated:YES];
    
    // Place a single pin
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    NSString *annotTitle = _address;
    [annotation setTitle:annotTitle]; //You can set the subtitle too
    [_incidentMapView addAnnotation:annotation];
    [_incidentMapView setSelectedAnnotations:@[annotation]];
    

}
/*
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString *identifier = @"CallLocation";
    
    if (_annotLat == nil) {
        
    }
    
    if ([annotation isKindOfClass:[CallLocation class]]) {
        
        CallLocation *location = (CallLocation *) annotation;
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [_incidentMap dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            
            
        } else {
            annotationView.annotation = annotation;
        }
        
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = NO;
        [annotationView setSelected:YES];
        
        
      
        
        
        return annotationView;
        
    }
    
    return nil;
    
}
*/

#pragma mark - Preview Actions

- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    
    // setup a list of preview actions
    UIPreviewAction *action1 = [UIPreviewAction actionWithTitle:@"Route to Incident" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Action 1 triggered");
        [self routeToIncidentFromUserLocation];
    }];
    
    UIPreviewAction *action2 = [UIPreviewAction actionWithTitle:@"Cancel" style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Destructive Action triggered");
    }];
    
    // add them to an arrary
    NSArray *actions = @[action1, action2];
    
    return actions;
    
}

-(void)routeToIncidentFromUserLocation {
    
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving,};
    
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake([_lat doubleValue], [_lon doubleValue]);
    
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    endingItem.name = @"Incident Location";
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];
    
}

@end
