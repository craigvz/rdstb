//
//  CallLocation.h
//  
//
//  Created by Craig VanderZwaag on 1/1/12.
//  Copyright (c) 2012 blueHulaStudios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface CallLocation : MKAnnotationView {
    
}

@property (copy) NSString *callType;
@property (copy) NSString *address;
@property (copy) NSString *city;
@property (copy) NSString *unit;
@property (copy) NSString *icon;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;


- (id)initWithName:(NSString*)name address:(NSString*)address city:(NSString*)city unit:(NSString*)unit icon:(NSString*)icon coordinate:(CLLocationCoordinate2D)coordinate;


@end
