//
//  NSString+Blank.h
//  CHPXMLParser
//
//  Created by Rob Fahrni on 1/9/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringBlank)
- (NSString*)stringByReplacingOccurrencesOfStringWithBlank:(NSString*)replaceThis;
@end
