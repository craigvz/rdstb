//
//  IncidentDetailCell.h
//  CHPXMLParser
//
//  Created by Craig VanderZwaag on 1/8/13.
//  Copyright (c) 2013 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncidentDetailCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *detailTime;
@property (nonatomic, strong) IBOutlet UILabel *detailText;


@end
