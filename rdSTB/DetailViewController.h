//
//  DetailViewController.h
//  BHTabViewSplitVC
//
//  Created by Craig Vanderzwaag on 9/21/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LocationManager.h"
#import <Parse/Parse.h> 



@interface DetailViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UINavigationControllerDelegate, LocationManagerDelegate>

@property (strong, nonatomic) id detailItem;



@property (nonatomic, strong) IBOutlet MKMapView *incidentMap;


@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *incNumber;
@property (nonatomic, strong) NSString *incAddress;
@property (nonatomic, strong) NSString *incCity;
@property (nonatomic, strong) NSString *incType;



@property (nonatomic, strong) NSNumber *annotLat;
@property (nonatomic, strong) NSNumber *annotLon;
@property (nonatomic, strong) NSString *detailAnnotCallType;
@property (nonatomic, strong) NSString *detailAnnotCallAddress;
@property (nonatomic, strong) NSString *annotIconStr;

@property (nonatomic, strong) IBOutlet UIImageView *gpsBack;
@property (nonatomic, strong) IBOutlet UIImageView *timeBack;


@end
