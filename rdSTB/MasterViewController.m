//
//  MasterViewController.m
//  BHTabViewSplitVC
//
//  Created by Craig Vanderzwaag on 9/21/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "CallInformationCell.h"
#import "CallLocation.h"
#import "UIColor+CustomColor.h"
#import "AppDelegate.h"
#import "PopViewController.h"

#define METERS_PER_MILE 1609.344


@interface MasterViewController () <UIViewControllerPreviewingDelegate>


@property (nonatomic, strong) NSString *addressToGeocode;

@end

@implementation MasterViewController

- (id)initWithCoder:(NSCoder *)aCoder {
    self = [super initWithCoder:aCoder];
    if (self) {
        
        UIStoryboard* sb;
        sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        // Customize the table
        
        // The className to query on
        self.parseClassName = @"IncidentDetails";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"incidentType";
        
        self.pullToRefreshEnabled = YES;
       
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 100;
        
    }
    return self;
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [[self.navigationController navigationBar] setBarStyle:UIBarStyleBlack];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.tableView setSeparatorColor:[UIColor colorWithR:53 G:53 B:53 A:1]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadObjects)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    self.tableView.estimatedRowHeight = 60;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.tableView reloadData];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
    
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if(indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[self.navigationController navigationBar] setTintColor:[UIColor whiteColor]];
    [[self.navigationController navigationBar] setTranslucent:YES];
    /*
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(162, 20, 36, 36)];
    imageView.image = [UIImage imageNamed:@"stbLogo2"];
    [self.navigationController.view addSubview:imageView];
     */
    [self check3DTouch];
    [self loadObjects];
    
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    
    [self check3DTouch];
    
    
}

- (void)check3DTouch {
    
    if ([self.traitCollection
         respondsToSelector:@selector(forceTouchCapability)] &&
        (self.traitCollection.forceTouchCapability ==
         UIForceTouchCapabilityAvailable))
    {
        [self registerForPreviewingWithDelegate:self sourceView:self.view];
         NSLog(@"3D Touch is available! Hurra!");
    }
    
    // register for 3D Touch (if available)
    
     else {
        
        NSLog(@"3D Touch is not available on this device. Sniff!");
        
        // handle a 3D Touch alternative (long gesture recognizer)
        //   self.longPress.enabled = YES;
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    NSLog(@"ObjectsDidLoad Called");
    
    // Tell the refresh control that we're done loading objects.
    if (NSClassFromString(@"UIRefreshControl")) {
        [self.refreshControl endRefreshing];
    }
    
    [self.tableView reloadData];
    
    //   NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:0 inSection:0];
    //    [self.tableView selectRowAtIndexPath:selectedCellIndexPath animated:false scrollPosition:UITableViewScrollPositionMiddle];
    //   [self tableView:self.tableView didSelectRowAtIndexPath:selectedCellIndexPath];
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)refreshControlValueChanged:(UIRefreshControl *)refreshControl {
    // The user just pulled down the table view. Start loading data.
    [self loadObjects];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row & 1) {
        cell.backgroundColor = [UIColor colorWithR:43 G:43 B:43 A:0.95];
    } else {
        cell.backgroundColor = [UIColor colorWithR:33 G:33 B:33 A:0.95];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"cell";
    NSLog(@"%@", object);
    
    CallInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CallInformationCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
    }
    
    NSLog(@"Row Height %f", self.tableView.rowHeight);
    
    _unitAssigned = [[object objectForKey:@"unitID"] stringByReplacingOccurrencesOfString:@"," withString:@""];
    _callAddress = [object objectForKey:@"address1"];
    _callCity = [object objectForKey:@"neighborhood"];
    _callTime = [object objectForKey:@"incidentTime"];
    _incNumber = [object objectForKey:@"incidentNumber"];
    _callType = [[object objectForKey:@"incidentType"] stringByReplacingOccurrencesOfString:@"*" withString:@""];
    _code20Text = [object objectForKey:@"code20"];
    _occupancyNumber = [object objectForKey:@"addressUnit"];
    _occupancyName = [object objectForKey:@"occupancyName"];
    NSString *completeAddress = [NSString stringWithFormat:@"%@\r%@\rUNIT: %@", _occupancyName, _callAddress, _occupancyNumber];
    NSString *addressWithUnit = [NSString stringWithFormat:@"%@\rUNIT: %@", _callAddress, _occupancyNumber];
    NSString *addressWithName = [NSString stringWithFormat:@"%@\r%@", _occupancyName, _callAddress];
    
    // Configure the cell
    
    if ([object objectForKey:@"incidentType"] != NULL) {

        cell.type.text = _callType;
        cell.incNumber.text = _incNumber;
        cell.time.text = _callTime;
        cell.unitID.text = _unitAssigned;
        if (_occupancyNumber && (![_occupancyName isEqualToString:@"-"])) {
            
            cell.address.text = completeAddress;
            
        } else if  (_occupancyNumber && ([_occupancyName isEqualToString:@"-"])) {
            
            cell.address.text = addressWithUnit;
        }
        
            else if ((![_occupancyName isEqualToString:@"-"]) && _occupancyNumber == nil)  {
            
            cell.address.text = addressWithName;
                
            } else {
                
                cell.address.text = _callAddress;
            }
    

        cell.city.text = _callCity;
        cell.unitID.hidden = NO;
        cell.city.hidden = NO;
        cell.incNumber.hidden = NO;
        
        if (_occupancyNumber == nil) {
            cell.occNumber.hidden = YES;
            cell.unitNumberLabel.hidden = YES;
        
        } else {
           
            cell.unitNumberLabel.hidden = NO;
            cell.occNumber.hidden = NO;
            cell.occNumber.text = _occupancyNumber;
        }
        
        
    } else {
        
        cell.type.text = @"Major Incident Information";
        cell.address.text = @"Code 20";
        cell.incNumber.hidden = YES;
      //  cell.time.text = [dateFormat stringFromDate:object.createdAt];
        cell.unitID.hidden = YES;
        cell.city.hidden = YES;
        NSLog(@"Code 20 Text --> %@", _code20Text);
        
    }
    
    if ([object objectForKey:@"incidentType"] != NULL) {
        
        NSString *callStr = _callType;
        if ([callStr localizedCaseInsensitiveContainsString:@"med"]) {
            UIImage *image = [UIImage imageNamed:@"logIconMed"];
            cell.icon.image = image;
        } else
            
            if ([callStr localizedCaseInsensitiveContainsString:@"fire"]) {
                if ([callStr localizedCaseInsensitiveContainsString:@"alarm"]) {
                    UIImage *image = [UIImage imageNamed:@"logIconAlarm"];
                    cell.icon.image = image;
                } else {
                    UIImage *image = [UIImage imageNamed:@"logIconFire"];
                    cell.icon.image = image;
                }
                
            } else
                
                if ([callStr localizedCaseInsensitiveContainsString:@"vehicle accident"] || [callStr localizedCaseInsensitiveContainsString:@"injury ta"]) {
                    UIImage *image = [UIImage imageNamed:@"logIconAccident"];
                    cell.icon.image = image;
                } else
                    
                    if ([callStr localizedCaseInsensitiveContainsString:@"assist"]) {
                        UIImage *image = [UIImage imageNamed:@"logIconAssist"];
                        cell.icon.image = image;
                    }
                    else
                    
                        if ([callStr localizedCaseInsensitiveContainsString:@"wires"]) {
                            UIImage *image = [UIImage imageNamed:@"logIconPower"];
                            cell.icon.image = image;
                        }
                        else {
                            
                        UIImage *image = [UIImage imageNamed:@"logIconAlarm"];
                        cell.icon.image = image;
                    }
        
        /*
         if ([callType  isEqualToString: @"Vehicle Accident"]) {
         cell.callType.text = NSLocalizedString(@"callTypeMVA", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_accident.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Structure Fire"]) {
         cell.callType.text = NSLocalizedString(@"callTypeSF", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_fire.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Vegetation Fire"]) {
         cell.callType.text = NSLocalizedString(@"callTypeWF", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_fire.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Alarm-Fire / Co Detector"]) {
         cell.callType.text = NSLocalizedString(@"callTypeAlarm", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_alarm.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Illegal Burn, Smoke Check"]) {
         cell.callType.text = NSLocalizedString(@"callTypeSmoke", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_fire.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Vehicle Fire - Passenger Car"]) {
         cell.callType.text = NSLocalizedString(@"callTypeVehFire", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_vehfire.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Mutual Aid - Other out of coun"]) {
         cell.callType.text = NSLocalizedString(@"callTypeMA", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Vehicle Accident Over the Side"]) {
         cell.callType.text = NSLocalizedString(@"callTypeMVAover", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_accident.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Public Assist - All"]) {
         cell.callType.text = NSLocalizedString(@"callTypePublicAssist", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Lines down/Wires Arcing"]) {
         cell.callType.text = NSLocalizedString(@"callTypeLinesDown", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_lines.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Fire-Reported Out"]) {
         cell.callType.text = NSLocalizedString(@"callTypeFireOut", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_fire.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Gas Leak Outside"]) {
         cell.callType.text = NSLocalizedString(@"callTypeGasOutside", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         if ([callType  isEqualToString: @"Single Engine Response - Defau"]) {
         cell.callType.text = NSLocalizedString(@"callTypeSingleEngine", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         
         
         if ([callType  isEqualToString: @"HazMat 1 Eng"]) {
         cell.callType.text = NSLocalizedString(@"callTypeHazMat1", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         
         if ([callType  isEqualToString: @"Fire-Trash/Dumpster/etc-Away"]) {
         cell.callType.text = NSLocalizedString(@"callTrashDumpsterAway", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         }
         
         if ([callType  isEqualToString: @"Rescue - Ocean/Surf"]) {
         cell.callType.text = NSLocalizedString(@"callTypeOceanRescue", nil);
         UIImage *image = [UIImage imageNamed:@"IncidentLogIcon_assist.png"];
         cell.callTypeIcon.image = image;
         
         
         } else {
         //   cell.callType.text = callType;
         }
         
         */
    }
    
    
    return cell;
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
       
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PFObject *incident = [self.objects objectAtIndex:indexPath.row];
        NSDate *object = self.objects[indexPath.row];
        
        NSLog(@"Object at index 0 -->:%@", [self.objects objectAtIndex:0]);
        
        _callAddress = [incident objectForKey:@"address1"];
        
         NSLog(@"Object at index 0 -->:%@", _callAddress);
        
        _callCity = [incident objectForKey:@"neighborhood"];
        _callTime = [incident objectForKey:@"incidentTime"];
        _incNumber = [incident objectForKey:@"incidentNumber"];
        _callType = [incident objectForKey:@"incidentType"];
        _unitAssigned = [incident objectForKey:@"unitID"];
        
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        if ([incident objectForKey:@"lat"] == nil) {
            if ([_callAddress containsString:@"HWY "]) {
                NSString *formattedAddress = [[[[[_callAddress stringByReplacingOccurrencesOfString:@"HWY " withString:@" U.S. "] stringByReplacingOccurrencesOfString:@" ON " withString:@" "] stringByReplacingOccurrencesOfString:@" OFF " withString:@" "] stringByReplacingOccurrencesOfString:@" NB " withString:@" "] stringByReplacingOccurrencesOfString:@" SB " withString:@" "];
                _addressToGeocode = [NSString stringWithFormat:@"%@,Santa Barbara,CA", formattedAddress];
                NSLog(@"Formatted Address %@", _addressToGeocode);
            } else {
                _addressToGeocode = [NSString stringWithFormat:@"%@,Santa Barbara,CA", _callAddress];
                NSLog(@"Formatted Address %@", _addressToGeocode);
                
            }
            
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:_addressToGeocode completionHandler:^(NSArray* placemarks, NSError* error){
                for (CLPlacemark* aPlacemark in placemarks)
                {
                    // Process the placemark.
                    NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
                    NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
                    _lat = [f numberFromString:latDest1];
                    _lon = [f numberFromString:lngDest1];
                    NSLog(@"Lat %@", _lat);
                    NSLog(@"Lng %@", _lon);
                    
                }
                DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
                
                controller.unit = [incident objectForKey:@"unitID"];
                controller.incAddress = [incident objectForKey:@"address1"];
                controller.time = [incident objectForKey:@"incidentTime"];
                controller.incNumber = [incident objectForKey:@"incidentNumber"];
                controller.incCity = [incident objectForKey:@"neighborhood"];
                controller.annotIconStr = _annotIconMarker;
                controller.annotLat = _lat;
                controller.annotLon = _lon;
                controller.incType = [incident objectForKey:@"incidentType"];
                [controller setDetailItem:object];
                controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
                controller.navigationItem.leftItemsSupplementBackButton = YES;
                
                for (id<MKAnnotation> annotation in controller.incidentMap.annotations) {
                    [controller.incidentMap removeAnnotation:annotation];
                }
                
                NSNumber * latitude = _lat;
                NSNumber * longitude = _lon;
                
                /*If needed
                NSString * name = _callType;
                NSString * address = _callAddress;
                NSString * city = _callCity;
                NSString * unit = _unitAssigned;
                */
                CLLocationCoordinate2D coordinate;
                coordinate.latitude = latitude.doubleValue;
                coordinate.longitude = longitude.doubleValue;
                
                MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
                
                [controller.incidentMap setRegion:viewRegion animated:YES];
                

            }];
            
        } else {
            
            _lat = [incident objectForKey:@"lat"];
            _lon = [incident objectForKey:@"lon"];
            
            DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
            controller.unit = _unitAssigned;
            controller.incAddress = _callAddress;
            controller.incCity = _callCity;
            controller.time = _callTime;
            controller.incNumber = _incNumber;
            controller.annotIconStr = _annotIconMarker;
            controller.annotLat = _lat;
            controller.annotLon = _lon;
            controller.incType = _callType;
            [controller setDetailItem:object];
            controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
            controller.navigationItem.leftItemsSupplementBackButton = YES;
            
            for (id<MKAnnotation> annotation in controller.incidentMap.annotations) {
                [controller.incidentMap removeAnnotation:annotation];
            }
            
            NSLog(@"Lat %@", _lat);
            NSLog(@"Lng %@", _lon);
            NSNumber * latitude = _lat;
            NSNumber * longitude = _lon;
            
            /*If needed
            NSString * name = _callType;
            NSString * address = _callAddress;
            NSString * city = _callCity;
            NSString * unit = _unitAssigned;
            */
            
            NSString *callStr = _callType;
            if ([callStr localizedCaseInsensitiveContainsString:@"med"]) {
                _annotIconMarker = @"med";
            } else
                
                if ([callStr localizedCaseInsensitiveContainsString:@"fire"]) {
                    _annotIconMarker = @"fire";
                } else
                    
                  //  if ([callStr localizedCaseInsensitiveContainsString:@"accident"]) {
                  //      _annotIconMarker = @"vehAcc";
                 //   } else
                        
                        if ([callStr localizedCaseInsensitiveContainsString:@"assist"]) {
                            _annotIconMarker = @"assist";
                        }
                        else {
                            _annotIconMarker = @"alert";
                        }
            controller.annotIconStr = _annotIconMarker;
            
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = latitude.doubleValue;
            coordinate.longitude = longitude.doubleValue;
            
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
            
            [controller.incidentMap setRegion:viewRegion animated:YES];
            
        }
    }
}

#pragma mark Peek and Pop

- (nullable UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    // check if we're not already displaying a preview controller
    if ([self.presentedViewController isKindOfClass:[PopViewController class]]) {
        return nil;
    }
    
    // shallow press: return the preview controller here (peek)
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    NSLog(@"INDEX PATH --> %@", indexPath);
    
    if (indexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        PFObject *incident = [self.objects objectAtIndex:indexPath.row];
        _object = self.objects[indexPath.row];
        
        _callAddress = [incident objectForKey:@"address1"];
        _lat = [incident objectForKey:@"lat"];
        _lon = [incident objectForKey:@"lon"];
        _callType = [incident objectForKey:@"incidentType"];
        _unitAssigned = [incident objectForKey:@"unitID"];
        _callTime = [incident objectForKey:@"incidentTime"];
        _callCity = [incident objectForKey:@"neighborhood"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PopViewController *previewController = [storyboard instantiateViewControllerWithIdentifier:@"pop"];
        previewController.lat = _lat;
        previewController.lon = _lon;
        previewController.address = _callAddress;
        
        previewingContext.sourceRect = cell.frame;
        
        return previewController;
    }
    
    return nil;
    
}


- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    
    // deep press: bring up the commit view controller (pop)
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailViewController *commitController = [storyboard instantiateViewControllerWithIdentifier:@"detail"];
    
    NSLog(@"Address-- %@", _callAddress);
    
    commitController.unit = _unitAssigned;
    commitController.incAddress = _callAddress;
    commitController.incCity = _callCity;
    commitController.time = _callTime;
    commitController.incNumber = _incNumber;
    commitController.annotLat = _lat;
    commitController.annotLon = _lon;
    commitController.incType = _callType;
    commitController.annotIconStr = _annotIconMarker;
    [commitController setDetailItem:_object];
    
    [self showViewController:commitController sender:self];
}

@end
