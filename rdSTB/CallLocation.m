//
//  CallLocation.m
//  KivaJSONDemo
//
//  Created by Craig VanderZwaag on 1/1/12.
//  Copyright (c) 2012 blueHulaStudios. All rights reserved.
//

#import "CallLocation.h"

@implementation CallLocation

@synthesize callType = _callType;
@synthesize address = _address;
@synthesize coordinate = _coordinate;
@synthesize city = _city;
@synthesize unit = _unit;
@synthesize icon = _icon;



- (id)initWithName:(NSString*)name address:(NSString*)address city:(NSString*)city unit:(NSString*)unit icon:(NSString*)icon coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        
        _callType = [name copy];
        _address = [address copy];
        _city = [city copy];
        _unit = [unit copy];
        _coordinate = coordinate;
        _icon = [icon copy];

    }
    return self;
}

- (NSString *)title {
    return _callType;
}

- (NSString *)subtitle {
    
    if (_city == nil) {
        
        NSString *combinedSubtitle = [NSString stringWithFormat:@"%@", _address];
        return combinedSubtitle;

    } else {
        
        NSString *combinedSubtitle = [NSString stringWithFormat:@"%@, %@", _address, _city];
        return combinedSubtitle;
        
    }
    
  // if needed
  //  return _address;
  //  return _city;
    
}

@end
