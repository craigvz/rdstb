//
//  DetailViewController.m
//  BHTabViewSplitVC
//
//  Created by Craig Vanderzwaag on 9/21/14.
//  Copyright (c) 2014 blueHula Studios. All rights reserved.
//


#define METERS_PER_MILE 1609.344


#import "DetailViewController.h"
#import "AppDelegate.h"
#import "CallLocation.h"
#import "UIColor+CustomColor.h"
//#import <GoogleMaps/GoogleMaps.h>

@interface DetailViewController ()

@property (nonatomic, strong) IBOutlet UILabel *unitLabel;
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) IBOutlet UILabel *incidentNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *incidentAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *incidentCityLabel;
@property (nonatomic, strong) IBOutlet UILabel *incidentTypeLabel;
@property (nonatomic, strong) IBOutlet UILabel *latLabel;
@property (nonatomic, strong) IBOutlet UILabel *lonLabel;
@property (nonatomic, strong) IBOutlet UILabel *currentLocationLabel;

@property (nonatomic, retain) NSNumber *lat;
@property (nonatomic, retain) NSNumber *lon;

@property (weak, nonatomic) IBOutlet UIView *gpsView;




@end

@implementation DetailViewController



#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        _unitLabel.text = _unit;
        _timeLabel.text = _time;
        _incidentNumberLabel.text = _incNumber;
        [self addIncidentAnnotation];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self queryIncidents];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearUIElements)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(queryIncidents)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    /*
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                            longitude:151.2086
                                                                 zoom:6];
    
    _mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    
    
    _googleIncidentMap = [GMSMapView mapWithFrame:CGRectZero camera:camera];

    
    
    _googleIncidentMap.delegate = self;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:34.420754
                                                            longitude:-119.698166
                                                                 zoom:6];
    _googleIncidentMap = [[GMSMapView alloc] init];
    _googleIncidentMap.camera = camera;
    _googleIncidentMap.myLocationEnabled = YES;
    _googleIncidentMap.mapType = kGMSTypeNormal;
    
    //Shows the compass button on the map
    
    _googleIncidentMap.settings.compassButton = YES;
    
    //Shows the my location button on the map
    
    _googleIncidentMap.settings.myLocationButton = YES;
    // Creates a marker in the center of the map.
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = _googleIncidentMap;
    

    _incidentMap.hidden = YES;
     
     */
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude =   34.420754;
    zoomLocation.longitude = -119.698166;
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05,0.20);
    
    MKCoordinateRegion region = MKCoordinateRegionMake(zoomLocation, span);
    
    _incidentMap.region = region;
    [_incidentMap setDelegate:self];

    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [LocationManager sharedManager].delegate = self;
   // [[self.navigationController navigationBar] setBarTintColor:[UIColor blueColor]];
   // [[self.navigationController navigationBar] setBarTintColor:[UIColor colorWithR:100 G:240 B:33 A:1]];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[self.navigationController navigationBar] setTintColor:[UIColor whiteColor]];
    [[self.navigationController navigationBar] setBarStyle:UIBarStyleBlack];
    [[self.navigationController navigationBar] setTranslucent:YES];
    
    _gpsView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clearUIElements {
    
    _incAddress = nil;
    NSLog(@"inc address %@", _incAddress);
    
}

-(void)queryIncidents {
    
    if (_incAddress == nil) {
        
        PFQuery *query = [PFQuery queryWithClassName:@"IncidentDetails"];
        [query orderByDescending:@"createdAt"];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!object) {
                NSLog(@"The getFirstObject request failed.");
            } else {
                // The find succeeded.
                NSLog(@"Successfully retrieved the object.");
                NSLog(@"Object %@", object);
                
                NSRange range = [[object objectForKey:@"unitID"] rangeOfString:@","];
                if (range.location != NSNotFound)
                {
                    NSArray *strings = [[object objectForKey:@"unitID"] componentsSeparatedByString:@","];
                    _unit = [[strings objectAtIndex:[strings count]-1] stringByReplacingOccurrencesOfString:@" " withString:@""];
                } else {
                    _unit = [[object objectForKey:@"unitID"] stringByReplacingOccurrencesOfString:@"," withString:@""];
                }
                
                _incAddress = [object objectForKey:@"address1"];
                _incCity = [object objectForKey:@"neighborhood"];
                _incNumber = [object objectForKey:@"incidentNumber"];
                _incType = [object objectForKey:@"incidentType"];
                _time = [object objectForKey:@"incidentTime"];
                
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                
                if ([object objectForKey:@"lat"] == nil) {
                    NSString *addressToGeocode = [NSString stringWithFormat:@"%@,Santa Barbara,CA", _incAddress];
                    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                    [geocoder geocodeAddressString:addressToGeocode completionHandler:^(NSArray* placemarks, NSError* error){
                        for (CLPlacemark* aPlacemark in placemarks)
                        {
                            // Process the placemark.
                            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
                            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
                            _lat = [f numberFromString:latDest1];
                            _lon = [f numberFromString:lngDest1];
                            _annotLat = _lat;
                            _annotLon = _lon;
                            NSLog(@"Lat %@", _lat);
                            NSLog(@"Lng %@", _lon);
                            
                        }
                        /*
                        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
                        controller.unit = _unit;
                        controller.incAddress = _incAddress;
                        controller.incCity = _incCity;
                        controller.time = _time;
                        controller.incNumber = _incNumber;
                        controller.annotLat = _lat;
                        controller.annotLon = _lon;
                        controller.incType = _incType;
                        [controller setDetailItem:object];
                        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
                        controller.navigationItem.leftItemsSupplementBackButton = YES;
                        */
                        for (id<MKAnnotation> annotation in _incidentMap.annotations) {
                            [_incidentMap removeAnnotation:annotation];
                        }
                        
                        NSNumber * latitude = _lat;
                        NSNumber * longitude = _lon;
                        NSString * name = _incType;
                        NSString * address = [_incAddress stringByReplacingOccurrencesOfString:@"  " withString:@""];
                        NSString * city = _incCity;
                        NSString * unit = _unit;
                        NSString * iconString = _incType;
                        
                        CLLocationCoordinate2D coordinate;
                        coordinate.latitude = latitude.doubleValue;
                        coordinate.longitude = longitude.doubleValue;
                        
                        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
                        
                        [_incidentMap setRegion:viewRegion animated:YES];
                        
                        CallLocation *annotation = [[CallLocation alloc] initWithName:name address:address city:city unit:unit icon:iconString coordinate:coordinate];
                        
                        
                        [_incidentMap performSelector:@selector(addAnnotation:) withObject:annotation afterDelay:1];
                        [_incidentMap performSelector:@selector(selectAnnotation:animated:) withObject:annotation afterDelay:1.5];
                        
                    }];
                    
                } else {
                    
                    _lat = [object objectForKey:@"lat"];
                    _lon = [object objectForKey:@"lon"];
                    _annotLat = _lat;
                    _annotLon = _lon;
                    /*
                    DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
                    controller.unit = _unit;
                    controller.incAddress = _incAddress;
                    controller.incCity = _incCity;
                    controller.time = _time;
                    controller.incNumber = _incNumber;
                    controller.annotLat = _lat;
                    controller.annotLon = _lon;
                    controller.incType = _incType;
                    [controller setDetailItem:object];

                    controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
                    controller.navigationItem.leftItemsSupplementBackButton = YES;
                    */
                    for (id<MKAnnotation> annotation in _incidentMap.annotations) {
                        [_incidentMap removeAnnotation:annotation];
                    }
                    
                    NSLog(@"Lat %@", _lat);
                    NSLog(@"Lng %@", _lon);
                    NSNumber * latitude = _lat;
                    NSNumber * longitude = _lon;
                    NSString * name = _incType;
                    NSString * address = [_incAddress stringByReplacingOccurrencesOfString:@"  " withString:@""];
                    NSString * city = _incCity;
                    NSString * unit = _unit;
                    NSString * iconString = _incType;
                    
                    CLLocationCoordinate2D coordinate;
                    coordinate.latitude = latitude.doubleValue;
                    coordinate.longitude = longitude.doubleValue;
                    
                    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
                    
                    [_incidentMap setRegion:viewRegion animated:YES];
                    
                    CallLocation *annotation = [[CallLocation alloc] initWithName:name address:address city:city unit:unit icon:iconString coordinate:coordinate];
                    NSLog(@"Annotation %@", annotation);
                    
                    [_incidentMap performSelector:@selector(addAnnotation:) withObject:annotation afterDelay:1];
                    [_incidentMap performSelector:@selector(selectAnnotation:animated:) withObject:annotation afterDelay:1.5];
                    
                }

                [self refreshUI];
             //   [self addIncidentAnnotation];
                
            }
        }];
    }
    
}

- (void)refreshUI
{
    // Update UI
    _unitLabel.text = _unit;
    _timeLabel.text = _time;
    _incidentNumberLabel.text = _incNumber;
    _incidentAddressLabel.text = _incAddress;
    _incidentCityLabel.text = _incCity;
    _incidentTypeLabel.text = _incType;
    
}


-(void)addIncidentAnnotation {
    
    for (id<MKAnnotation> annotation in _incidentMap.annotations) {
        [_incidentMap removeAnnotation:annotation];
    }
    
    
    NSNumber * latitude = _annotLat;
    NSNumber * longitude = _annotLon;
    NSString * name = _incType;
    
    NSString * address = _incAddress;
    address = [address substringToIndex:[address length] - 2];
    NSString * city = _incCity;
    NSString * unit = _unit;
    NSString * iconString = _incType;
    
    NSLog(@"Type %@, Add %@", name, address);
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude.doubleValue;
    coordinate.longitude = longitude.doubleValue;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    [_incidentMap setRegion:viewRegion animated:YES];
    
    CallLocation *annotation = [[CallLocation alloc] initWithName:name address:address city:city unit:unit icon:iconString coordinate:coordinate];
    
    
    [_incidentMap performSelector:@selector(addAnnotation:) withObject:annotation afterDelay:1];
    [_incidentMap performSelector:@selector(selectAnnotation:animated:) withObject:annotation afterDelay:1.5];
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString *identifier = @"CallLocation";
    
    if (_annotLat == nil) {
        
    }
    
    if ([annotation isKindOfClass:[CallLocation class]]) {
            CallLocation *location = (CallLocation *) annotation;
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [_incidentMap dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            
            
        } else {
            annotationView.annotation = annotation;
        }
        
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
        [annotationView setSelected:YES];
        
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 34, 19)];
        [button setImage:[UIImage imageNamed:@"16-car.png"] forState:UIControlStateNormal];
        annotationView.rightCalloutAccessoryView = button;
        
        
        if ([location.icon  localizedCaseInsensitiveContainsString:@"med"]) {
            annotationView.pinColor = MKPinAnnotationColorPurple;
            
            UIImage *image = [UIImage imageNamed:@"logIconMed"];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
            imgView.frame = CGRectMake(0,0,40,40);
            annotationView.leftCalloutAccessoryView = imgView;
        } else if ([location.icon  localizedCaseInsensitiveContainsString:@"fire"]) {
            UIImage *image = [UIImage imageNamed:@"logIconFire"];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
            imgView.frame = CGRectMake(0,0,40,40);
            annotationView.leftCalloutAccessoryView = imgView;
            
        }
        else if ([location.icon  localizedCaseInsensitiveContainsString:@"Vehicle Accident"]) {
            UIImage *image = [UIImage imageNamed:@"logIconAccident"];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
            imgView.frame = CGRectMake(0,0,40,40);
            annotationView.leftCalloutAccessoryView = imgView;
            
        }
        
        else if ([location.icon  localizedCaseInsensitiveContainsString:@"assist"]) {
            UIImage *image = [UIImage imageNamed:@"logIconAssist"];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
            imgView.frame = CGRectMake(0,0,40,40);
            annotationView.leftCalloutAccessoryView = imgView;
            
        }
        else {
            
            UIImage *image = [UIImage imageNamed:@"logIconAlarm"];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
            imgView.frame = CGRectMake(0,0,40,40);
            annotationView.leftCalloutAccessoryView = imgView;
            
        }
        
        return annotationView;
        
    }
    
    return nil;
    
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake([_annotLat doubleValue], [_annotLon doubleValue]);
    
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    endingItem.name = @"Incident Location";
    
    
    NSString *mapPrefValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"map_preference"];
    int i = [mapPrefValue intValue];
    
    
    if (i == 0) {
        
        
        
        if([endingItem respondsToSelector:@selector(openInMapsWithLaunchOptions:)])
        {
            //using iOS6 native maps app
            NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
            [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
            
            [endingItem openInMapsWithLaunchOptions:launchOptions];
            
        } else {
            
            //using iOS 5 which has the Google Maps application
            NSString *url = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", mapView.userLocation.location.coordinate.latitude, mapView.userLocation.location.coordinate.longitude, [_annotLat doubleValue], [_annotLon doubleValue]];
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
        
        
    } else {
        
        if (i == 1) {
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                NSString *url = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f", mapView.userLocation.location.coordinate.latitude, mapView.userLocation.location.coordinate.longitude, [_annotLat doubleValue], [_annotLon doubleValue]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                
                
            } else {
                
                //using iOS 5 which has the Google Maps application
                NSString *url = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", mapView.userLocation.location.coordinate.latitude, mapView.userLocation.location.coordinate.longitude, [_annotLat doubleValue], [_annotLon doubleValue]];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            }
        }
    }
    
}


- (BOOL)openMapsWithItems:(NSArray *)mapItems launchOptions:(NSDictionary *)launchOptions{
    [MKMapItem openMapsWithItems:mapItems launchOptions:launchOptions];
    return YES;
    
    
}

- (void)locationUpdate:(CLLocation*)location {
    
    NSLog(@"LOCATION--> %f", location.coordinate.latitude);
    
    NSString *latitudeString = [NSString stringWithFormat:@"%lf\u00B0", location.coordinate.latitude];
    NSArray *coorParts = [latitudeString componentsSeparatedByString:@"."];
    float conversionString = [[coorParts objectAtIndex:1] floatValue];
    NSString *convertedCoord = [NSString stringWithFormat:@"%.4lf", conversionString/1000000 * 60];
    //   NSLog(@"DecMin Coord -->%@", convertedCoord);
    NSString *finalConvertedLat = [NSString stringWithFormat:@"%@\u00B0 %@", [coorParts objectAtIndex:0], convertedCoord];
    //   NSLog(@"Final Coord -->%@", finalConvertedLat);
    
    _latLabel.text = finalConvertedLat;
    
    NSString *longString = [NSString stringWithFormat:@"%lf\u00B0", location.coordinate.longitude];
    NSArray *coorPartsLong = [longString componentsSeparatedByString:@"."];
    float conversionStringLong = [[coorPartsLong objectAtIndex:1] floatValue];
    NSString *convertedCoordLong = [NSString stringWithFormat:@"%.4lf", conversionStringLong/1000000 * 60];
    //   NSLog(@"DecMin Coord -->%@", convertedCoordLong);
    NSString *finalConvertedLong = [NSString stringWithFormat:@"%@\u00B0 %@", [coorPartsLong objectAtIndex:0], convertedCoordLong];
    //   NSLog(@"Final Coord -->%@", finalConvertedLong);
    _lonLabel.text = finalConvertedLong;
    
    //  NSLog(@"currentLocation Dispatch");
}

#pragma mark - UIPopoverPresentationControllerDelegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationFullScreen;
}

// This will wrap the content view controller in a navigation controller when diplayed in full screen.
- (UIViewController *)presentationController:(UIPresentationController *)controller viewControllerForAdaptivePresentationStyle:(UIModalPresentationStyle)style {
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller.presentedViewController];
    return navController;
}






@end
